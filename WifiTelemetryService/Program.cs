﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace WifiTelemetryService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            if (Environment.UserInteractive)
            {
                WifiTelemetryService debug_instance = new WifiTelemetryService();
                debug_instance.StartConsole(args);

                while (true)
                {
                    Thread.Sleep(5000);
                }
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                new WifiTelemetryService()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}

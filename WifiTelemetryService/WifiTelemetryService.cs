﻿using System;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Configuration;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Sockets;
using System.Net;
using SnmpSharpNet;
using System.IO;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

namespace WifiTelemetryService
{

    public class OldDataCounter
    {
        public OldDataCounter(Double oldup, Double olddn)
        {
            OldDataUploaded = oldup;
            OldDataDownloaded = olddn;
        }

        public Double OldDataUploaded;
        public Double OldDataDownloaded;
    }

    public class WiFiInfo
    {
        public String OID;
        public String IPAddress;
        public String MACAddress;
        public String SerialNo;
        public String Description;
        public Int32 ConnectedClients;
        public Double NewDataUploaded;
        public Double NewDataDownloaded;
        public Double OldDataUploaded;
        public Double OldDataDownloaded;

        public WiFiInfo()
        {
            OID = "";
            IPAddress = "";
            MACAddress = "";
            SerialNo = "";
            Description = "";
            ConnectedClients = 0;
            NewDataUploaded = 0;
            NewDataDownloaded = 0;
            OldDataUploaded = 0;
            OldDataDownloaded = 0;
        }
    }

    public class wifi_data
    {
        public double new_value_data_downloaded;
        public double new_value_data_uploaded;

        public double old_value_data_downloaded;
        public double old_value_data_uploaded;

        public double data_download;
        public double data_upload;

        public long users_connected_count;
        public string mac_address;
        public long timestamp;
        public long sampling_rate;
    }

    public class wifi_event
    {
        public string event_name;
        public string event_parameter;
        public long timestamp;
    }

    public class data_outer_envelope
    {
        public string application_id;
        public long message_type;
        public long qos;
        public string nsid;
        public long transport_type;
        public long timestamp;
        public wifi_data[] payload;
    }

    public class event_outer_envelope
    {
        public string application_id;
        public long message_type;
        public long qos;
        public string nsid;
        public long transport_type;
        public long timestamp;
        public wifi_event[] payload;
    }

    public class wifi_data_payload
    {
        public data_outer_envelope[] records;
    }
    public class wifi_event_payload
    {
        public event_outer_envelope[] records;
    }

    public partial class WifiTelemetryService : ServiceBase
    {
        Thread SNMPTrapThread = null;
        Thread SNMPQueryThread = null;
        String PostUrl = null;
        String HttpUser = null;
        String HttpPass = null;

        static string LogDirectory = null;
        static Int32 LogFileSizeMB = 10;
        static string DirLog = null;
        static string DirBackup = null;
        static string SNMPServer = null;

        Dictionary<String, WiFiInfo> APNList = new Dictionary<string, WiFiInfo>();
        Dictionary<String, String> KeyValuePairs = new Dictionary<string, string>();
        Dictionary<String, String> MACtoDescriptionList = new Dictionary<string, string>();

        String DeviceModel = null;

        Int32 SamplingRate = 300;

        static object ThreadLock = new object();

        public WifiTelemetryService()
        {
            InitializeComponent();

            PostUrl = ConfigurationManager.AppSettings["PostUrl"];
            HttpUser = ConfigurationManager.AppSettings["HttpUser"];
            HttpPass = ConfigurationManager.AppSettings["HttpPass"];
            LogDirectory = ConfigurationManager.AppSettings["LogDirectory"];
            LogFileSizeMB = Convert.ToInt32(ConfigurationManager.AppSettings["LogFileSizeMB"]);
            SNMPServer = ConfigurationManager.AppSettings["SNMPServer"];
            SamplingRate = Convert.ToInt32(ConfigurationManager.AppSettings["SamplingRate"]);

            if (Directory.Exists(LogDirectory) == false)
            {
                Directory.CreateDirectory(LogDirectory);
            }

            DirLog = LogDirectory + "\\Log\\";
            DirBackup = LogDirectory + "\\Backup\\";

            if (Directory.Exists(DirLog) == false)
            {
                Directory.CreateDirectory(DirLog);
            }

            if (Directory.Exists(DirBackup) == false)
            {
                Directory.CreateDirectory(DirBackup);
            }

            String filename_log = DirLog + Assembly.GetExecutingAssembly().GetName().Name + ".log";
            String filename_backup = DirBackup + Assembly.GetExecutingAssembly().GetName().Name + DateTime.Now.ToString("yyyyMMddhhmmss") + ".log";

            FileInfo fi = new FileInfo(filename_log);

            if (fi.Exists)
            {
                File.Delete(filename_log);
            }
        }

        protected override void OnStart(string[] args)
        {
            StartConsole(args);
        }

        protected override void OnStop()
        {
        }
        public void StartConsole(string[] args)
        {
            Read("1.3.6.1.4.1.25053.1.2.1.1.1.1.9", "Model", ref DeviceModel);

            Read("1.3.6.1.4.1.25053.1.2.2.1.1.2.1.1.2", "Description by OID", ref KeyValuePairs);

            foreach(var kv in KeyValuePairs)
            {
                WiFiInfo info = new WiFiInfo();
                info.OID = kv.Key;
                info.Description = kv.Value;

                if (info.Description.Length > 0)
                {
                    APNList.Add(info.OID, info);
                }
            }

            Read("1.3.6.1.4.1.25053.1.2.2.1.1.2.1.1.1", "Per APN MAC Address by OID", ref KeyValuePairs);

            foreach(var kv in KeyValuePairs)
            {
                if(APNList.ContainsKey(kv.Key))
                {
                    var info = APNList[kv.Key];
                    info.MACAddress = kv.Value;
                    info.MACAddress = info.MACAddress.ToUpper();
                    info.MACAddress = info.MACAddress.Replace(" ", "");
                    APNList[kv.Key] = info;
                }
            }

            Read("1.3.6.1.4.1.25053.1.2.2.1.1.2.1.1.10", "Per APN IP Address OID", ref KeyValuePairs);

            foreach (var kv in KeyValuePairs)
            {
                if (APNList.ContainsKey(kv.Key))
                {
                    var info = APNList[kv.Key];
                    info.IPAddress = kv.Value;
                    APNList[kv.Key] = info;
                }
            }

            Read("1.3.6.1.4.1.25053.1.2.2.1.1.2.1.1.5", "Per APN Serial No. by OID", ref KeyValuePairs);

            foreach (var kv in KeyValuePairs)
            {
                if (APNList.ContainsKey(kv.Key))
                {
                    var info = APNList[kv.Key];
                    info.SerialNo = kv.Value;
                    APNList[kv.Key] = info;
                }
            }

            Read("1.3.6.1.4.1.25053.1.2.2.1.1.2.1.1.15", "Connected clients per APN", ref KeyValuePairs);

            foreach (var kv in KeyValuePairs)
            {
                if (APNList.ContainsKey(kv.Key))
                {
                    var info = APNList[kv.Key];
                    info.ConnectedClients = Convert.ToInt32(kv.Value);
                    APNList[kv.Key] = info;
                }
            }

            Read("1.3.6.1.4.1.25053.1.2.2.1.1.2.1.1.21", "Bytes received per APN", ref KeyValuePairs);

            foreach (var kv in KeyValuePairs)
            {
                if (APNList.ContainsKey(kv.Key))
                {
                    var info = APNList[kv.Key];
                    info.OldDataDownloaded = Convert.ToDouble(kv.Value);
                    APNList[kv.Key] = info;
                }
            }

            Read("1.3.6.1.4.1.25053.1.2.2.1.1.2.1.1.25", "Bytes sent per APN", ref KeyValuePairs);

            foreach (var kv in KeyValuePairs)
            {
                if (APNList.ContainsKey(kv.Key))
                {
                    var info = APNList[kv.Key];
                    info.OldDataUploaded = Convert.ToDouble(kv.Value);
                    APNList[kv.Key] = info;
                }
            }

            foreach (var obj in APNList)
            {
                MACtoDescriptionList.Add(obj.Value.MACAddress, obj.Value.Description);
            }

            this.SNMPTrapThread = new Thread(this.ReceiveTrap);
            this.SNMPTrapThread.IsBackground = true;
            this.SNMPTrapThread.Start();

            WriteLog("Started trap receiver thread");

            this.SNMPQueryThread = new Thread(this.ReceiveQuery);
            this.SNMPQueryThread.IsBackground = true;
            this.SNMPQueryThread.Start();

            WriteLog("Started MIB polling thread");
        }

        private void ReceiveTrap()
        {
            // Construct a socket and bind it to the trap manager port 162 
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 162);
            EndPoint ep = (EndPoint)ipep;
            socket.Bind(ep);
            // Disable timeout processing. Just block until packet is received 
            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 0);
            bool run = true;
            int inlen = -1;

            while (run)
            {
                byte[] indata = new byte[16 * 1024];
                // 16KB receive buffer int inlen = 0;
                IPEndPoint peer = new IPEndPoint(IPAddress.Any, 0);
                EndPoint inep = (EndPoint)peer;
                try
                {
                    inlen = socket.ReceiveFrom(indata, ref inep);

                    if (inlen > 0)
                    {
                        // Check protocol version int 
                        int ver = SnmpPacket.GetProtocolVersion(indata, inlen);
                        if (ver == (int)SnmpVersion.Ver1)
                        {
                            WriteLog("Error: SNMP Version 1 Trap is not supported");
                        }
                        else
                        {
                            // Parse SNMP Version 2 TRAP packet 
                            SnmpV2Packet pkt = new SnmpV2Packet();
                            pkt.decode(indata, inlen);
                            if ((SnmpSharpNet.PduType)pkt.Pdu.Type != PduType.V2Trap)
                            {
                                WriteLog("Error: Received packet is not SNMP PDU");
                                //Error
                            }
                            else
                            {
                                foreach (Vb v in pkt.Pdu.VbList)
                                {
                                    WriteLog(v.Oid.ToString() + " " + v.Value.ToString());

                                    string event_name = "";
                                    string event_param = "";

                                    // APN available
                                    if(v.Oid.ToString() == "1.3.6.1.4.1.25053.2.2.1.35")
                                    {
                                        event_name = "apn_available";
                                        event_param = v.Value.ToString();
                                    }

                                    // APN lost
                                    if (v.Oid.ToString() == "1.3.6.1.4.1.25053.2.2.1.5")
                                    {
                                        event_name = "apn_lost";
                                        event_param = v.Value.ToString();
                                    }

                                    // APN heart beat lost
                                    if (v.Oid.ToString() == "1.3.6.1.4.1.25053.2.2.1.6")
                                    {
                                        event_name = "apn_heart_beat_lost";
                                        event_param = v.Value.ToString();
                                    }

                                    if(event_name.Length < 1)
                                    {
                                        continue;
                                    }

                                    String mac = event_param;
                                    mac = mac.ToUpper();
                                    mac = mac.Replace(" ", "");

                                    if (!MACtoDescriptionList.ContainsKey(mac))
                                    {
                                        continue;
                                    }

                                    wifi_event_payload controller_event = new wifi_event_payload();

                                    controller_event.records = new event_outer_envelope[1];
                                    controller_event.records[0] = new event_outer_envelope();

                                    controller_event.records[0].payload = new wifi_event[1];
                                    controller_event.records[0].payload[0] = new wifi_event();

                                    controller_event.records[0].nsid = MACtoDescriptionList[mac];
                                    controller_event.records[0].transport_type = 2;
                                    DateTime device_time = DateTime.Now;
                                    device_time = DateTime.SpecifyKind(device_time, DateTimeKind.Utc);
                                    DateTimeOffset device_time_offset = device_time;
                                    controller_event.records[0].timestamp = device_time_offset.ToUnixTimeMilliseconds();
                                    controller_event.records[0].application_id = DeviceModel;
                                    controller_event.records[0].message_type = 0;
                                    controller_event.records[0].qos = 1;
                                    controller_event.records[0].payload[0].event_name = event_name;
                                    controller_event.records[0].payload[0].event_parameter = event_param;

                                    DateTime sensor_time = DateTime.Now;
                                    sensor_time = DateTime.SpecifyKind(sensor_time, DateTimeKind.Utc);
                                    DateTimeOffset sensor_time_offset = sensor_time;
                                    controller_event.records[0].payload[0].timestamp = sensor_time_offset.ToUnixTimeMilliseconds();

                                    string serializedJson = JsonConvert.SerializeObject(controller_event);

                                    var client = new HttpClient();
                                    var data = new StringContent(serializedJson, Encoding.UTF8, "application/json");
                                    var response = client.PostAsync(PostUrl, data);
                                    var result = response.Result.Content.ReadAsStringAsync();
                                }
                            }
                        }
                    } //
                    else
                    {
                        if (inlen == 0)
                        {
                            WriteLog("Error: Zero length packet rceived");
                        }
                    }                }
                catch (Exception ex)
                {
                    WriteLog(ex.Message);
                    inlen = -1;
                }

                WriteLog("Socket opened for traps");
            }
        }

        private void ReceiveQuery()
        {
            Thread.Sleep(SamplingRate * 1000);

            while (true)
            {
                Read("1.3.6.1.4.1.25053.1.2.2.1.1.2.1.1.15", "Connected clients per APN", ref KeyValuePairs);

                foreach (var kv in KeyValuePairs)
                {
                    if (APNList.ContainsKey(kv.Key))
                    {
                        var info = APNList[kv.Key];
                        info.ConnectedClients = Convert.ToInt32(kv.Value);
                        APNList[kv.Key] = info;
                    }
                }

                Read("1.3.6.1.4.1.25053.1.2.2.1.1.2.1.1.21", "Bytes received per APN", ref KeyValuePairs);

                foreach (var kv in KeyValuePairs)
                {
                    if (APNList.ContainsKey(kv.Key))
                    {
                        var info = APNList[kv.Key];
                        info.NewDataDownloaded = Convert.ToDouble(kv.Value);
                        APNList[kv.Key] = info;
                    }
                }

                Read("1.3.6.1.4.1.25053.1.2.2.1.1.2.1.1.25", "Bytes sent per APN", ref KeyValuePairs);

                foreach (var kv in KeyValuePairs)
                {
                    if (APNList.ContainsKey(kv.Key))
                    {
                        var info = APNList[kv.Key];
                        info.NewDataUploaded = Convert.ToDouble(kv.Value);
                        APNList[kv.Key] = info;
                    }
                }

                Dictionary<String, OldDataCounter> oldcounts = new Dictionary<string, OldDataCounter>();

                foreach (var info in APNList)
                {
                    WriteLog("Creating telemetry for " + info.Value.IPAddress);

                    wifi_data_payload wf = new wifi_data_payload();

                    wf.records = new data_outer_envelope[1];
                    wf.records[0] = new data_outer_envelope();

                    wf.records[0].payload = new wifi_data[1];
                    wf.records[0].payload[0] = new wifi_data();
                    wf.records[0].payload[0].sampling_rate = SamplingRate;

                    wf.records[0].nsid = info.Value.Description;
                    wf.records[0].transport_type = 2;
                    DateTime device_time = DateTime.Now;
                    device_time = DateTime.SpecifyKind(device_time, DateTimeKind.Local);
                    DateTimeOffset device_time_offset = device_time;
                    wf.records[0].timestamp = device_time_offset.ToUnixTimeMilliseconds();
                    wf.records[0].application_id = DeviceModel;
                    wf.records[0].message_type = 0;
                    wf.records[0].qos = 1;

                    wf.records[0].payload[0].new_value_data_uploaded = info.Value.NewDataUploaded;
                    wf.records[0].payload[0].new_value_data_downloaded = info.Value.NewDataDownloaded;
                    wf.records[0].payload[0].old_value_data_uploaded = info.Value.OldDataUploaded;
                    wf.records[0].payload[0].old_value_data_downloaded = info.Value.OldDataDownloaded;

                    WriteLog("Current sampling rate is " + Convert.ToString(SamplingRate) + " seconds");

                    WriteLog(info.Value.IPAddress + " Old up " + Convert.ToString(wf.records[0].payload[0].old_value_data_uploaded) + " New Up " + Convert.ToString(wf.records[0].payload[0].new_value_data_uploaded) + " Old dn " + Convert.ToString(wf.records[0].payload[0].old_value_data_downloaded) + " New dn " + Convert.ToString(wf.records[0].payload[0].new_value_data_downloaded));

                    if(wf.records[0].payload[0].new_value_data_uploaded <= wf.records[0].payload[0].old_value_data_uploaded)
                    {
                        wf.records[0].payload[0].data_upload = 0;
                        WriteLog(info.Value.IPAddress + " Computed up bytes => New value is less or equal to old => " + Convert.ToString(wf.records[0].payload[0].data_upload));
                    }
                    else
                    {
                        wf.records[0].payload[0].data_upload = wf.records[0].payload[0].new_value_data_uploaded - wf.records[0].payload[0].old_value_data_uploaded;
                        WriteLog(info.Value.IPAddress + " Computed up bytes (new up - old up) => " + Convert.ToString(wf.records[0].payload[0].data_upload));
                    }

                    if (wf.records[0].payload[0].new_value_data_downloaded <= wf.records[0].payload[0].old_value_data_downloaded)
                    {
                        wf.records[0].payload[0].data_download = 0;
                        WriteLog(info.Value.IPAddress + " Computed dn  bytes => New value is less or equal to old => " + Convert.ToString(wf.records[0].payload[0].data_download));
                    }
                    else
                    {
                        wf.records[0].payload[0].data_download = wf.records[0].payload[0].new_value_data_downloaded - wf.records[0].payload[0].old_value_data_downloaded;
                        WriteLog(info.Value.IPAddress + " Computed dn bytes (new dn - old dn) => " + Convert.ToString(wf.records[0].payload[0].data_download));
                    }

                    info.Value.OldDataUploaded = info.Value.NewDataUploaded;
                    info.Value.OldDataDownloaded = info.Value.NewDataDownloaded;

                    oldcounts.Add(info.Key, new OldDataCounter(info.Value.OldDataUploaded, info.Value.OldDataDownloaded));

                    wf.records[0].payload[0].users_connected_count = info.Value.ConnectedClients;
                    wf.records[0].payload[0].mac_address = info.Value.MACAddress.ToUpper();
                    DateTime sensor_time = DateTime.Now;
                    sensor_time = DateTime.SpecifyKind(sensor_time, DateTimeKind.Local);
                    DateTimeOffset sensor_time_offset = sensor_time;
                    wf.records[0].payload[0].timestamp = sensor_time_offset.ToUnixTimeMilliseconds();

                    string serializedJson = JsonConvert.SerializeObject(wf);

                    var client = new HttpClient();
                    var data = new StringContent(serializedJson, Encoding.UTF8, "application/json");

                    try
                    {
                        var response = client.PostAsync(PostUrl, data);
                        var httpresult = response.Result.Content.ReadAsStringAsync();
                    }
                    catch (Exception ex)
                    {
                        WriteLog(ex.Message);
                    }

                    Thread.Sleep(1000);
                }

                foreach (var kv in oldcounts)
                {
                    if (APNList.ContainsKey(kv.Key))
                    {
                        var info = APNList[kv.Key];
                        info.OldDataUploaded = Convert.ToDouble(kv.Value.OldDataUploaded);
                        info.OldDataDownloaded = Convert.ToDouble(kv.Value.OldDataDownloaded);
                        APNList[kv.Key] = info;
                    }
                }

                GC.Collect();

                Thread.Sleep(SamplingRate * 1000);

            }
        }

        private void Read(String oidstr, String description, ref Dictionary<String, String> kvlist)
        {
            kvlist.Clear();

            OctetString community = new OctetString("public");
            AgentParameters param = new AgentParameters(community);
            param.Version = SnmpVersion.Ver2;
            IpAddress agent = new IpAddress(SNMPServer);
            UdpTarget target = new UdpTarget((IPAddress)agent, 161, 2000, 1);
            Oid rootOid = new Oid(oidstr);
            Oid lastOid = (Oid)rootOid.Clone();
            Pdu pdu = new Pdu(PduType.GetNext);

            while (lastOid != null)
            {
                if (pdu.RequestId != 0)
                {
                    pdu.RequestId += 1;
                }

                pdu.VbList.Clear();
                pdu.VbList.Add(lastOid);

                SnmpV2Packet result = (SnmpV2Packet)target.Request(pdu, param);

                if (result != null)
                {
                    if (result.Pdu.ErrorStatus != 0)
                    {
                        WriteLog("Error in SNMP reply. Error " + Convert.ToString(result.Pdu.ErrorStatus) + " index " + Convert.ToString(result.Pdu.ErrorIndex));
                        lastOid = null;
                        break;
                    }
                    else
                    {
                        foreach (Vb v in result.Pdu.VbList)
                        {
                            if (rootOid.IsRootOf(v.Oid))
                            {
                                String extra = v.Oid.ToString().Replace(oidstr + ".", "").Trim();
                                kvlist.Add(extra, v.Value.ToString());
                                lastOid = v.Oid;
                            }
                            else
                            {
                                lastOid = null;
                            }
                        }
                    }
                }
                else
                {
                    WriteLog("No response received from SNMP agent.");
                }
            }
            target.Close();
        }

        private void Read(String oidstr, String description, ref String strval)
        {
            strval = "";

            OctetString community = new OctetString("public");
            AgentParameters param = new AgentParameters(community);
            param.Version = SnmpVersion.Ver2;
            IpAddress agent = new IpAddress(SNMPServer);
            UdpTarget target = new UdpTarget((IPAddress)agent, 161, 2000, 1);
            Oid rootOid = new Oid(oidstr);
            Oid lastOid = (Oid)rootOid.Clone();
            Pdu pdu = new Pdu(PduType.GetNext);

            while (lastOid != null)
            {
                if (pdu.RequestId != 0)
                {
                    pdu.RequestId += 1;
                }

                pdu.VbList.Clear();
                pdu.VbList.Add(lastOid);

                SnmpV2Packet result = (SnmpV2Packet)target.Request(pdu, param);

                if (result != null)
                {
                    if (result.Pdu.ErrorStatus != 0)
                    {
                        WriteLog("Error in SNMP reply. Error " + Convert.ToString(result.Pdu.ErrorStatus) + " index " + Convert.ToString(result.Pdu.ErrorIndex));
                        lastOid = null;
                        break;
                    }
                    else
                    {
                        foreach (Vb v in result.Pdu.VbList)
                        {
                            if (rootOid.IsRootOf(v.Oid))
                            {
                                strval = v.Value.ToString();
                                lastOid = v.Oid;
                            }
                            else
                            {
                                lastOid = null;
                            }
                        }
                    }
                }
                else
                {
                    WriteLog("No response received from SNMP agent.");
                }
            }
            target.Close();
        }

        private void WriteLog(string str)
        {
            lock (ThreadLock)
            {
                String filename_log = DirLog + Assembly.GetExecutingAssembly().GetName().Name + ".log";
                String filename_backup = DirBackup + Assembly.GetExecutingAssembly().GetName().Name + DateTime.Now.ToString("yyyyMMddhhmmss") + ".log";

                FileInfo fi = new FileInfo(filename_log);

                if (fi.Exists)
                {
                    if (fi.Length > 1024 * 1024 * LogFileSizeMB)
                    {
                        File.Move(filename_log, filename_backup);
                    }
                }
                File.AppendAllText( filename_log, DateTime.Now.ToString() + " " + str + "\r\n");
            }
        }
    }
}
